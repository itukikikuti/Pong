#include <string>
#include "XLibrary11.hpp"
using namespace std;
using namespace XLibrary11;

int MAIN()
{
	Camera camera;
	camera.color = Float4(0.0f, 0.0f, 0.0f, 1.0f);

	Sprite ballSprite(L"ball.png");
	Float3 ballSpeed(5.0f, 0.0f, 0.0f);

	Sprite playerSprite(L"ball.png");
	playerSprite.scale.y = 5.0f;

	Float3 player1Position(-300.0f, 0.0f, 0.0f);
	Float3 player2Position(300.0f, 0.0f, 0.0f);
	int player1Score = 0;
	int player2Score = 0;

	Text player1ScoreText(L"0", 10.0f);
	player1ScoreText.position = Float3(-100.0f, 200.0f, 0.0f);
	player1ScoreText.scale = 10.0f;
	player1ScoreText.color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

	Text player2ScoreText(L"0", 10.0f);
	player2ScoreText.position = Float3(100.0f, 200.0f, 0.0f);
	player2ScoreText.scale = 10.0f;
	player2ScoreText.color = Float4(1.0f, 1.0f, 1.0f, 1.0f);

	Sound hitSound(L"hit.wav");
	Sound pointSound(L"point.wav");

	while (App::Refresh())
	{
		camera.Update();

		ballSprite.position += ballSpeed;

		if (ballSprite.position.y > App::GetWindowSize().y / 2.0f)
		{
			ballSpeed.y = -fabsf(ballSpeed.y);
			hitSound.Play();
		}
		if (ballSprite.position.y < -App::GetWindowSize().y / 2.0f)
		{
			ballSpeed.y = fabsf(ballSpeed.y);
			hitSound.Play();
		}

		if (ballSprite.position.x > App::GetWindowSize().x / 2.0f)
		{
			player1Score++;
			player1ScoreText.Create(to_wstring(player1Score), 10.0f);
			ballSprite.position = Float3(0.0f, 0.0f, 0.0f);
			ballSpeed.x = -ballSpeed.x;
			ballSpeed.y = 0.0f;
			pointSound.Play();
		}
		if (ballSprite.position.x < -App::GetWindowSize().x / 2.0f)
		{
			player2Score++;
			player2ScoreText.Create(to_wstring(player2Score), 10.0f);
			ballSprite.position = Float3(0.0f, 0.0f, 0.0f);
			ballSpeed.x = -ballSpeed.x;
			ballSpeed.y = 0.0f;
			pointSound.Play();
		}

		ballSprite.Draw();

		if (App::GetKey('R'))
		{
			player1Position.y += 5.0f;
		}
		if (App::GetKey('F'))
		{
			player1Position.y -= 5.0f;
		}

		if (player1Position.y > App::GetWindowSize().y / 2.0f)
		{
			player1Position.y = App::GetWindowSize().y / 2.0f;
		}
		if (player1Position.y < -App::GetWindowSize().y / 2.0f)
		{
			player1Position.y = -App::GetWindowSize().y / 2.0f;
		}

		if (player1Position.x - 16.0f < ballSprite.position.x &&
			player1Position.x + 16.0f > ballSprite.position.x &&
			player1Position.y - 48.0f < ballSprite.position.y &&
			player1Position.y + 48.0f > ballSprite.position.y)
		{
			ballSpeed.x = 5.0f;
			ballSpeed.y = (ballSprite.position.y - player1Position.y) * 0.2f;
			hitSound.Play();
		}

		playerSprite.position = player1Position;
		playerSprite.Draw();

		if (App::GetKey('U'))
		{
			player2Position.y += 5.0f;
		}
		if (App::GetKey('J'))
		{
			player2Position.y -= 5.0f;
		}

		if (player2Position.y > App::GetWindowSize().y / 2.0f)
		{
			player2Position.y = App::GetWindowSize().y / 2.0f;
		}
		if (player2Position.y < -App::GetWindowSize().y / 2.0f)
		{
			player2Position.y = -App::GetWindowSize().y / 2.0f;
		}

		if (player2Position.x - 16.0f < ballSprite.position.x &&
			player2Position.x + 16.0f > ballSprite.position.x &&
			player2Position.y - 48.0f < ballSprite.position.y &&
			player2Position.y + 48.0f > ballSprite.position.y)
		{
			ballSpeed.x = -5.0f;
			ballSpeed.y = (ballSprite.position.y - player2Position.y) * 0.2f;
			hitSound.Play();
		}

		playerSprite.position = player2Position;
		playerSprite.Draw();

		player1ScoreText.Draw();
		player2ScoreText.Draw();
	}

	return 0;
}
